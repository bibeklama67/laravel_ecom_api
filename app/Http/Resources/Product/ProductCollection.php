<?php

namespace App\Http\Resources\Product;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return[
            'name'=>$this->name,
           
            'price'=>$this->price,
            'stock'=>$this->stock==0?'Out of stock':$this->stock,
            'discount'=>$this->discount,
            'totalPrice'=>round((1-($this->discount/100)) * $this->price,2),
            'rating'=>$this->reviews->count() ? round($this->reviews->sum('star')/$this->reviews->count(),2):'No ratings yet',
            'href' => [
                'reviews'=>route('products.show',$this->id)
            ]
        ];
    }
}
