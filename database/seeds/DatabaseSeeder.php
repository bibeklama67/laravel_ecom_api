<?php

use App\Model\Review;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
      factory(App\Model\Product::class,50)->create();
      factory(Review::class,300)->create();
    }
}
